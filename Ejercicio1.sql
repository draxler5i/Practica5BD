USE Practica2 ;

CREATE FUNCTION f_promedios (@valor1 INT, @valor2 INT, @valor3 INT) RETURNS INT
AS
BEGIN
DECLARE @resultado INT
SET @resultado = (@valor1 + @valor2 + @valor3) /3
RETURN @resultado
END

SELECT dbo.f_promedios(12,12,18)