USE Practica2 ;

CREATE FUNCTION UpperCaseNombre (@persona_id Varchar(20)) RETURNS Varchar(20)
AS

BEGIN

DECLARE @nombre Varchar(20)

SELECT @nombre = (UPPER(nombre)) FROM Persona WHERE persona_id = @persona_id

RETURN @nombre

END  

-- personda_id = 10
SELECT dbo.UpperCaseNombre(10)