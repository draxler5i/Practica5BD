USE Practica2 ;

CREATE FUNCTION EdadPersona(@persona_id int, @Fec_Actual Datetime = '03/09/2018') RETURNS INT
AS

BEGIN

DECLARE @Edad INT

SELECT @Edad = Datediff(yy,fechaNacimiento,@Fec_Actual) FROM Persona WHERE persona_id = @persona_id

RETURN @Edad

END

--Edad de la persona con ID 5
SELECT dbo.EdadPersona(5, default)
